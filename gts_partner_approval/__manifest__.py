
{
    'name': 'Customer/Supplier Approval | Partner Validation Process',
    'version': '12.0.0.0.1',
    'category': 'Sales',
    'author': 'Geo Technosoft',
    'website': 'www.geotechnosoft.com',
    'license': 'AGPL-3',
    'sequence': 1,
    'summary': ''' Module to allow approval of partners ''',
    'description': '''
        This module would allow you to validate partner or to validate customer or vendor
        before using anywhere in the transaction like sale, purchase, invoice.
        Partner would be created in draft state and once the approval manager approves it then 
        it would be available for transaction.
        Approval vendor, vendor approval workflow, partner Approval odoo 11,
        vendor Validation odoo confirm vendor Validate
        vendor approve vendor workflow vendor approval flow vendor approval odoo 11
        Approval customer, customer approval workflow, partner validate,
        customer Validation odoo confirm customer
        customer approve customer workflow customer approval flow
        Approval supplier, supplier approval workflow, supplier validate,
        supplier Validation odoo confirm supplier
        supplier approve supplier workflow supplier approval flow odoo v11 odoo version 11
        Vendor Validation Process Supplier Validation Process Customer Validation Process odoo 11
        Customer/Vendor Approval Customer Vendor Approval Customer or Vendor Approval
    ''',
    'depends': [
        'base', 'contacts', 'mail', 'base', 'purchase', 'sale'
    ],
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'data/sequence_data_view.xml',
        'data/res_company_data.xml',
        'data/res_users_data.xml',
        'data/template_data_view.xml',
        'views/res_partner_state_field_views.xml',
        'views/company_views.xml',
        'views/partner_views.xml',
        'views/purchase_views.xml',
        'views/sale_views.xml',
        'views/invoice_views.xml',
        'views/account_invoice_view.xml',
    ],
    'demo': [
        # 'demo/res.partner.state_field.csv'
    ],
    "images": ['static/description/banner.png'],
    "license": 'AGPL-3',
    "price": 25.00,
    "currency": "EUR",
    "installable": True,
    "auto_install": False,
    "application": True,
}
